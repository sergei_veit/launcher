import { getContent } from '../../reducers/content';
import { connect } from 'react-redux';
import { QUERY_TYPES } from '../../utils';
import Container from '../../helpers/ResultContainer';

const mapStateToProps = ({ content }) => {
    return {
        ...content
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: () => getContent(dispatch, QUERY_TYPES.upcoming)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
