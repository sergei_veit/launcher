import { getPastContent } from '../../reducers/pastContent';
import { connect } from 'react-redux';
import { QUERY_TYPES } from '../../utils';
import Container from '../../helpers/ResultContainer';

const mapStateToProps = ({ pastContent }) => {
    return {
        ...pastContent
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: () => getPastContent(dispatch, QUERY_TYPES.past)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
