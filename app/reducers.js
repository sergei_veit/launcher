import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import content from './reducers/content';
import pastContent from './reducers/pastContent';

const rootReducer = combineReducers({
    routing,
    content,
    pastContent
});

export default rootReducer;
