import axios from 'axios';
import { host } from '../utils';

export const PAST_CONTENT = 'PAST_CONTENT';

const pastContentReducer = (state = null, action) => {
    switch (action.type) {
        case PAST_CONTENT:
            return { ...state, ...action.payload };
        default:
            return { ...state };
    }
};

export function getPastContent(dispatch, type) {
    dispatch({
        type: PAST_CONTENT,
        payload: {
            isLoading: true,
            isError: false,
            data: null
        }
    });
    axios.get(host + type).then(({ data }) => {
        dispatch({
            type: PAST_CONTENT,
            payload: {
                isLoading: false,
                isError: false,
                data
            }
        });
    }).catch(() => {
        dispatch({
            type: PAST_CONTENT,
            payload: {
                isLoading: false,
                isError: true,
                data: null
            }
        });
    });
}


export default pastContentReducer;
