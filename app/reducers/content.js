import axios from 'axios';
import { host } from '../utils';

export const CONTENT = 'CONTENT';

const contentReducer = (state = null, action) => {
    switch (action.type) {
        case CONTENT:
            return { ...state, ...action.payload };
        default:
            return { ...state };
    }
};

export function getContent(dispatch, type) {
    dispatch({
        type: CONTENT,
        payload: {
            isLoading: true,
            isError: false,
            data: null
        }
    });
    axios.get(host + type).then(({ data }) => {
        dispatch({
            type: CONTENT,
            payload: {
                isLoading: false,
                isError: false,
                data
            }
        });
    }).catch(() => {
        dispatch({
            type: CONTENT,
            payload: {
                isLoading: false,
                isError: true,
                data: null
            }
        });
    });
}


export default contentReducer;
