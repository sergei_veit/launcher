import React from 'react';
import ReactTable from 'react-table';
import Header from '../components/Header';
import { includesFilterBy } from './ArrayHelpers';

class Container extends React.Component {
    constructor(props) {
        super(props);

        if (!this.props.data) {
            this.props.getData();
        }

        this.state = {
            firstStage: '',
            secondStage: '',
            siteNameFilter: ''
        };
    }

    openStage(firstStage, secondStage) {
        this.setState({
            firstStage,
            secondStage
        });
    }

    getColumns(data) {
        if (data === null || !data.length) {
            return [];
        }
        return Object.keys(data[0]).map((key) => {
            if (key === 'cores') {
                return ({
                    width: 65,
                    Cell: row => {
                        return (
                            <span
                                onClick={() => {
                                    this.openStage(row.original.cores, row.original.payloads);
                                }}
                            >&#8594;</span>
                        );
                    },
                    style: {
                        cursor: 'pointer',
                        fontSize: 25,
                        padding: '0',
                        textAlign: 'center',
                        userSelect: 'none'
                    },
                    Header: 'Stages',
                    accessor: 'cores'
                });
            }
            if (key === 'payloads') {
                return null;
            }

            return ({
                Header: this.capitalizeFirstLetter(key.replace(/_/g, ' ')),
                accessor: key
            });
        }).filter(item => item !== null);
    }

    getNoDataString() {
        if (this.props.isLoading) {
            return 'Please wait... We are loading data.';
        }
        if (this.props.isError) {
            return 'Oops, something went wrong.';
        }

        return 'Oops, no data.';
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    updateValue(value) {
        if (value === null) {
            return 'No data';
        }
        if (Array.isArray(value)) {
            return value.join(', ');
        }
        if (typeof(value) === 'object') {
            return this.convertObjectArray([value]);
        }
        return value.toString();
    }

    convertObjectArray(array) {
        return array.map((item, itemIndex) => {
            const keys = Object.keys(item);

            return (
                <ul className="item-list" key={itemIndex}>
                    {keys.map((key, index) => {
                        return (
                            <li key={index}>
                                {this.capitalizeFirstLetter(key.replace(/_/g, ' '))}
                                {': '}
                                {this.updateValue(item[key])}
                            </li>
                        );
                    })}
                </ul>
            );
        });
    }

    convertArrayToItem(array, type) {
        if (type === 'cores' || type === 'payloads') {
            return this.convertObjectArray(array);
        }

        return array.join(', ');
    }

    getFlatObject(item) {
        let result = {};
        const keys = Object.keys(item);

        keys.forEach((key) => {
            if (Array.isArray(item[key])) {
                result = ({
                    ...result,
                    [key]: this.convertArrayToItem(item[key], key)
                });
            } else if (typeof(item[key]) === 'object' && item[key] !== null) {
                result = ({
                    ...result,
                    ...this.getFlatObject(item[key])
                });
            } else {
                result[key] = this.updateValue(item[key]);
            }
        });

        return result;
    }

    getData() {
        return this.props.data
            ? this.props.data.map((item) => {
                return this.getFlatObject(item);
            })
            : [];
    }

    search(value) {
        if (this.state.siteNameFilter.trim() !== value.trim()) {
            this.setState({
                siteNameFilter: value
            });
        }
    }

    render() {
        let data = this.getData();
        if (this.state.siteNameFilter) {
            data = includesFilterBy(data, 'site_name', this.state.siteNameFilter, false);
        }
        const columns = this.getColumns(data);

        return (
            <div className="page-container">
                <Header
                    onSearch={this.search.bind(this)}
                />
                <div className="table-container">
                    <ReactTable
                        data={data}
                        columns={columns}
                        noDataText={this.getNoDataString()}
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />
                </div>
                {(this.state.firstStage || this.state.secondStage) && (
                    <div className="stages-container">
                        <div>
                            <h3>First stage</h3>
                            {this.state.firstStage}
                        </div>
                        <div>
                            <h3>Second stage</h3>
                            {this.state.secondStage}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

Container.propTypes = {
    data: React.PropTypes.any,
    isLoading: React.PropTypes.bool,
    isError: React.PropTypes.bool,
    getData: React.PropTypes.func
};

export default Container;
