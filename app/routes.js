import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Upcoming from './pages/Upcoming';
import Past from './pages/Past';

export default (
    <Switch>
        <Route exact path="/" component={Upcoming} />
        <Route path="/past" component={Past} />
    </Switch>
);
