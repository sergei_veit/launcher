import React from 'react';
import { NavLink } from 'react-router-dom';

class Header extends React.Component {
    onKeyDown(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            const value = e.target.value.trim();
            this.props.onSearch && this.props.onSearch(value);
        }

        return true;
    }

    onChange(e) {
        const value = e.target.value.trim();
        if (value === '') {
            this.props.onSearch && this.props.onSearch(value);
        }
    }

    render() {
        return (
            <header>
                <div className="menu">
                    <NavLink
                        exact
                        to="/"
                        activeClassName="active"
                    >
                        Upcoming
                    </NavLink>
                    <NavLink
                        exact
                        to="/past"
                        activeClassName="active"
                    >
                        Past
                    </NavLink>
                </div>
                <div className="search">
                    <input
                        type="search"
                        placeholder="Enter here site name..."
                        onKeyDown={this.onKeyDown.bind(this)}
                        onChange={this.onChange.bind(this)}
                    />
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    onSearch: React.PropTypes.func
};

export default Header;
