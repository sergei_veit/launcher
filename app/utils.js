export const host = 'https://api.spacexdata.com/v2/';

export const QUERY_TYPES = {
    past: 'launches',
    upcoming: 'launches/upcoming'
};
